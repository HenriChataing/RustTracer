
use point::*;
use vector::*;

pub mod sphere;
pub mod plane;

pub struct Line {
    pub origin: Point,
    pub direction: Vector3,
}

pub trait Geometry {
    fn intersect(&self, ray: &Line) -> Option<f64>;
    fn normal(&self, hit: &Point) -> Vector3;
}
