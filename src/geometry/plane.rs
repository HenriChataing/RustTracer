
use geometry::*;

pub struct Plane {
    pub origin: Point,
    pub normal: Vector3,
}

impl Geometry for Plane {
    fn intersect(&self, ray: &Line) -> Option<f64> {
        let normal = &self.normal;
        let denom = normal.dot(&ray.direction);
        if denom > 1e-6 {
            let v = self.origin - ray.origin;
            let distance = v.dot(&normal) / denom;
            if distance >= 0.0 {
                Some(distance)
            } else {
                None
            }
        } else {
            None
        }
    }

    fn normal(&self, _: &Point) -> Vector3 {
        -self.normal
    }
}
