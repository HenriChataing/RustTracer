use scene::*;
use point::Point;
use vector::Vector3;
use geometry::Line;
use color::*;
use std::f64::consts::PI;

use random::Source;

type Ray = Line;

const BLACK : Color = Color { red: 0.0, green: 0.0, blue: 0.0 };

pub fn render(scene: &mut Scene) -> Vec<u8> {
    let mut image : Vec<u8> = vec![0; (scene.height * scene.width * 3) as usize];

    for x in 0..scene.width {
        for y in 0..scene.height {

            let mut color = BLACK;
            // Get samples of colors which will be mixed
            for _ in 0..scene.nb_samples {
                let ray = Ray::create_prime(x, y, scene);
                color = color + scene.cast_ray(&ray, 0);
            }

            let color = (color * (1. / (scene.nb_samples as f64))).to_rgba();
            let pixel = ((y * scene.width + x) * 3) as usize;

            for i in 0 .. 3 {
                image[pixel + i] = color[i];
            }
        }
    }
    image
}

impl Ray {
    pub fn create_prime(x: u32, y: u32, scene: &mut Scene) -> Ray {
        let aspect_ratio = (scene.width as f64) / (scene.height as f64);
        let fov_adjustment = (scene.fov.to_radians() / 2.).tan();

        let rng_x = scene.rng.read::<f64>();
        let rng_y = scene.rng.read::<f64>();

        let sensor_x = (((x as f64 + rng_x) / scene.width as f64) * 2.0 - 1.0)
                        * aspect_ratio * fov_adjustment;
        let sensor_y = 1.0 - ((y as f64 + rng_y) / scene.height as f64) * 2.0
                        * fov_adjustment;
        Ray {
            origin: Point { x:0., y:0., z:0. },
            direction: Vector3 {
                x: sensor_x,
                y: sensor_y,
                z: -1.0,
            }.normalize(),
        }
    }

    pub fn reflect(normal: Vector3, incident: Vector3) -> Vector3 {
        incident - (2.0 * incident.dot(&normal)) * normal
    }

    pub fn refract(normal: Vector3,
                   incident: Vector3,
                   ior: f64) -> Vector3 {
        let mut cosi = normal.dot(&incident);
        let (eta, norm) =
            if cosi < 0. {
                // Outside the surface, cos(theta) should be positive.
                cosi = -cosi;
                (1. / ior, normal)
            } else {
                // Inside the surface, cos(theta) is positive, but reverse the
                // normal direction.
                (ior, -normal)
            };
        let k = 1. - eta * eta * (1. - cosi * cosi);
        if k < 0. {
            // Under the critical angle, total internal reflection.
            Vector3::zero()
        } else {
            eta * incident + (eta * cosi - k.sqrt()) * norm
        }
    }

    /// Compute ratio of reflected light using fresnel equations.
    pub fn fresnel(normal: Vector3,
                   incident: Vector3,
                   ior: f64) -> f64 {

        let mut cosi = normal.dot(&incident);
        let (etai, etat) =
            if cosi < 0. {
                cosi = -cosi;
                (1., ior)
            } else {
                (ior, 1.)
            };

        // Compute sini using Snell's law
        let sint = etai / etat * (1. - cosi * cosi).sqrt();
        // Total internal reflection
        if sint >= 1. {
            1.
        } else {
            let cost = (1. - sint * sint).sqrt();
            let rs = ((etat * cosi) - (etai * cost)) / ((etat * cosi) + (etai * cost));
            let rp = ((etai * cosi) - (etat * cost)) / ((etai * cosi) + (etat * cost));
            (rs * rs + rp * rp) / 2.
            // As a consequence of the conservation of energy,
            // transmittance is given by: kt = 1 - kr;
        }
    }
}

impl<'a> Scene<'a> {
    pub fn trace(&self, ray: &Ray) -> Option<(&'a Element, f64)> {
        let mut index : isize = -1;
        let mut distance = 0.0;
        for i in 0..self.elements.len() {
            if let Some(d) = self.elements[i].geometry.intersect(&ray) {
                if index == -1 || d < distance {
                    index = i as isize;
                    distance = d;
                }
            }
        }
        if index == -1 { None } else {
            Some((&self.elements[index as usize], distance)) }
    }

    pub fn shade_diffuse(&self, ray: &Ray, elt: &Element, distance: f64) -> Color {

        let mut color = Color { red : 0.0, green: 0.0, blue: 0.0 };
        let hit_point = ray.origin + (ray.direction * distance);
        let surface_normal = elt.geometry.normal(&hit_point);

        for light in &self.lights { match light.typ {
            LightType::Directed(direction) => {
                let dir_to_light = -direction;
                let shadow_ray = Ray {
                    origin: hit_point + (surface_normal * self.shadow_bias),
                    direction: dir_to_light,
                };
                let in_light = self.trace(&shadow_ray).is_none();

                let light_power = if in_light {
                    (surface_normal.dot(&dir_to_light) as f64).max(0.0)
                    * light.intensity
                } else {
                    0.0
                };
                let light_reflected = elt.material.albedo / PI;

                let light_color = light.color * light_power * light_reflected;

                color = color + elt.material.color * light_color;
            },
            LightType::Spherical(origin) => {
                let dir_to_light = (origin - hit_point).normalize();
                let shadow_ray = Ray {
                    origin: hit_point + (surface_normal * self.shadow_bias),
                    direction: dir_to_light,
                };
                let distance2 = (origin - hit_point).norm();
                let in_light = if let Some((_, d)) = self.trace(&shadow_ray) {
                    d * d > distance2
                } else {
                    true
                };

                let light_power = if in_light {
                    (surface_normal.dot(&dir_to_light) as f64).max(0.0)
                    * light.intensity / (4.0 * PI * distance2 as f64)
                } else {
                    0.0
                };
                let light_reflected = elt.material.albedo / PI;

                let light_color = light.color * light_power * light_reflected;

                color = color + elt.material.color * light_color;
            }
        }}

        color.clamp()
    }

    pub fn get_color(&self, ray: &Ray, elt: &Element, distance: f64, depth: u32) -> Color {

        use scene::SurfaceType::*;

        let hit_point = ray.origin + (ray.direction * distance);
        let normal = elt.geometry.normal(&hit_point);

        let mut diffuse_color = self.shade_diffuse(ray, elt, distance);

        match elt.material.typ {
            // Reflective surface, mix diffuse light with reflected light.
            Reflect(r) => {
                let reflect_dir = Ray::reflect(normal, ray.direction);
                let reflect_ray = Ray {
                    origin: hit_point + normal * self.shadow_bias,
                    direction: reflect_dir
                };
                let reflect_color = self.cast_ray(&reflect_ray, depth + 1);
                diffuse_color * (1. - r) + reflect_color * r
            },
            // Transparent object, with reflection and refraction.
            ReflectAndRefract(ior) => {
                let mut refract_color = BLACK;
                // Compute fresnel
                let kr = Ray::fresnel(normal, ray.direction, ior);
                let outside = ray.direction.dot(&normal) < 0.;
                let bias = self.shadow_bias * normal;

                // Compute refraction if it is not a case of total
                // internal reflection.
                if kr < 1. {
                    let refract_dir = Ray::refract(normal, ray.direction, ior).normalize();
                    let refract_ori = if outside { hit_point - bias } else { hit_point + bias };
                    let refract_ray = Ray { origin: refract_ori, direction: refract_dir };
                    refract_color = self.cast_ray(&refract_ray, depth + 1);
                }

                // Compute reflection.
                let reflect_dir = Ray::reflect(normal, ray.direction).normalize();
                let reflect_ori = if outside { hit_point + bias } else { hit_point - bias };
                let reflect_ray = Ray { origin: reflect_ori, direction: reflect_dir };
                let reflect_color = self.cast_ray(&reflect_ray, depth + 1);

                // Mix the colors from reflected and refracted light.
                // diffuse_color + reflect_color * kr + refract_color * (1. - kr)
                reflect_color * kr + refract_color * (1. - kr)
            },
            // Diffuse surface.
            _ => diffuse_color
        }
    }

    pub fn cast_ray(&self, ray: &Ray, depth: u32) -> Color {
        if depth >= self.max_recursion_depth {
            BLACK
        } else if let Some((elt, d)) = self.trace(ray) {
            self.get_color(ray, elt, d, depth)
        } else {
            BLACK
        }
    }
}
