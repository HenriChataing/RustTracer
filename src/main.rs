mod scene;
mod renderer;
mod vector;
mod geometry;

#[macro_use]
mod point;

#[macro_use]
mod color;

extern crate random;

use renderer::render;
use scene::*;
use point::Point;
use color::Color;
use vector::Vector3;

use geometry::sphere::*;
use geometry::plane::*;

use std::fs::File;
use std::io::Write;

fn main() {

    let elements = vec![
        Element {
            geometry: &Sphere {
                center: point!(-4.0, 0.0, -4.0),
                radius: 1.0,
            },
            material: Material {
                color: color::RED,
                albedo: 1.0,
                typ: SurfaceType::Diffuse,
            }
        },
        Element {
            geometry: &Sphere {
                center: point!(-1.5, 2.0, -6.0),
                radius: 0.5,
            },
            material: Material {
                color: color::GREEN,
                albedo: 1.0,
                typ: SurfaceType::Diffuse,
            }
        },
        Element {
            geometry: &Sphere {
                center: point!(3.0, 2.0, -8.0),
                radius: 3.0,
            },
            material: Material {
                color: color::WHITE,
                albedo: 1.0,
                typ: SurfaceType::ReflectAndRefract(1.3),
            }
        },
        Element {
            geometry: &Sphere {
                center: point!(-2.0, 2.0, -11.0),
                radius: 2.0,
            },
            material: Material {
                color: color::BLUE,
                albedo: 1.0,
                typ: SurfaceType::Diffuse,
            }
        },
        /*Element::Plane(Plane {
            origin: Point {
                x: 0.0,
                y: 0.0,
                z: 1.0

            },
            normal: Vector3 {
                x: 0.0,
                y: 0.0,
                z: 1.0,
            },
            material: Material {
                color: Color {
                    red: 1.0,
                    green: 1.0,
                    blue: 1.0,
                },
                albedo: 1.0,
                typ: SurfaceType::Reflect(0.7),
            },
        }),*/
        Element {
            geometry: &Plane {
                origin: point!(0.0, -1.0, -0.0),
                normal: Vector3 {
                    x: 0.0,
                    y: -1.0,
                    z: 0.0,
                },
            },
            material: Material {
                color: color::WHITE,
                albedo: 1.0,
                typ: SurfaceType::Diffuse,
            }
        },
    ];

    let mut scene = Scene {
        width: 800,
        height: 600,
        fov: 95.0,
        elements: elements,
        lights: vec![
            Light {
                typ: LightType::Spherical(point!(0.0, 15.0, -5.0)),
                color: color::WHITE,
                intensity: 10000.0,
            },
            /*Light {
                typ: LightType::Directed(Vector3{ x: 2.0, y: -0.25, z: -3.0 }.normalize()),
                color: Color { red : 0.5, green: 1.0, blue: 0.0 },
                intensity: 2.0,
            },*/

        ],
        shadow_bias: 1e-13,
        max_recursion_depth: 32,
        nb_samples: 4,
        rng: random::default(),
    };

    let image_buf = &render(&mut scene);

    let mut file = File::create("img.ppm").unwrap();
    let header = format!("P6 {} {} 255\n", scene.width, scene.height);
    file.write(header.as_bytes());
    file.write(image_buf);
}
