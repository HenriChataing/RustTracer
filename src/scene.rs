
use point::Point;
use vector::Vector3;
use color::Color;
use geometry::Geometry;

use random;

#[derive(Copy, Clone)]
pub enum SurfaceType {
    Diffuse,
    Reflect(f64),
    ReflectAndRefract(f64),
}

pub struct Material {
    pub color: Color,
    pub albedo: f64,
    pub typ: SurfaceType,
}

pub struct Element<'a> {
    pub geometry: &'a Geometry,
    pub material: Material,
}

pub enum LightType {
    Directed(Vector3),
    Spherical(Point),
}

pub struct Light {
    pub typ: LightType,
    pub color: Color,
    pub intensity: f64,
}

pub struct Scene<'a> {
    pub width:  u32,
    pub height: u32,
    pub fov:    f64,
    pub elements: Vec<Element<'a>>,
    pub lights: Vec<Light>,
    pub shadow_bias: f64,
    pub max_recursion_depth: u32,
    pub nb_samples: usize,
    pub rng: random::Default,
}
