
use std::ops::{Add, Mul};

const GAMMA: f64 = 2.2;

fn gamma_encode(linear: f64) -> f64 {
    linear.powf(1.0 / GAMMA)
}

#[derive(Copy, Clone)]
pub struct Color {
    pub red:    f64,
    pub green:  f64,
    pub blue:   f64,
}

macro_rules! color {
    ($r:expr, $g:expr, $b:expr) => (Color { red: $r, green: $g, blue: $b })
}

pub const BLACK: Color = color!(0., 0., 0.);
pub const WHITE: Color = color!(1., 1., 1.);
pub const RED: Color = color!(1., 0., 0.);
pub const GREEN: Color = color!(0., 1., 0.);
pub const BLUE: Color = color!(0., 0., 1.);

impl Color {
    pub fn clamp(&self) -> Color {
        Color {
            red: self.red.min(1.0).max(0.0),
            blue: self.blue.min(1.0).max(0.0),
            green: self.green.min(1.0).max(0.0),
        }
    }

    pub fn to_rgba(&self) -> [u8;3] {
        [(gamma_encode(self.red) * 255.) as u8,
         (gamma_encode(self.green) * 255.) as u8,
         (gamma_encode(self.blue) * 255.) as u8
        ]
    }
}

impl Mul for Color {
    type Output = Color;

    fn mul(self, other: Color) -> Color {
        Color {
            red: self.red * other.red,
            blue: self.blue * other.blue,
            green: self.green * other.green,
        }
    }
}

impl Mul<f64> for Color {
    type Output = Color;

    fn mul(self, other: f64) -> Color {
        Color {
            red: self.red * other,
            blue: self.blue * other,
            green: self.green * other,
        }
    }
}

impl Mul<Color> for f64 {
    type Output = Color;
    fn mul(self, other: Color) -> Color {
        other * self
    }
}

impl Add for Color {
    type Output = Color;
    fn add(self, other: Color) -> Color {
        Color {
            red: self.red + other.red,
            blue: self.blue + other.blue,
            green: self.green + other.green,
        }
    }
}
